package com.acf.teamrss.util;

import com.acf.teamrss.R;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class Constants {

    public static final Map<Integer, Object[]> NEWSPAPERS_MAP;
    static {
        Map<Integer, Object[]> aMap = new LinkedHashMap<>();

        aMap.put(R.string.diario_de_sevilla, new Object[] {R.string.diario_de_sevilla, R.mipmap.diario_de_sevilla, "http://www.diariodesevilla.es/rss/articles.php?sec=1886"});
        aMap.put(R.string.estadio_deportivo, new Object[] {R.string.estadio_deportivo, R.mipmap.estadio_deportivo, "http://www.estadiodeportivo.com/elementosInt/rss/3"});
        aMap.put(R.string.marca, new Object[] {R.string.marca, R.mipmap.marca, "http://estaticos.marca.com/rss/futbol_equipos_sevilla.xml"});
        aMap.put(R.string.as, new Object[] {R.string.as, R.mipmap.as, "http://masdeporte.as.com/tag/rss/sevilla_futbol_club/a"});
        aMap.put(R.string.mundo_deportivo, new Object[] {R.string.mundo_deportivo, R.mipmap.mundo_deportivo, "http://www.mundodeportivo.com/feed/rss/futbol/sevilla"});

        NEWSPAPERS_MAP = Collections.unmodifiableMap(aMap);
    }
}
