package com.acf.teamrss.util;

import com.acf.teamrss.R;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class Constants {

    public static final Map<Integer, Object[]> NEWSPAPERS_MAP;
    static {
        Map<Integer, Object[]> aMap = new LinkedHashMap<>();

        aMap.put(R.string.marca, new Object[] {R.string.marca, R.mipmap.marca, "http://estaticos.marca.com/rss/futbol_equipos_atletico.xml"});
        aMap.put(R.string.as, new Object[] {R.string.as, R.mipmap.as, "http://masdeporte.as.com/tag/rss/atletico_madrid/a"});
        aMap.put(R.string.mundo_deportivo, new Object[] {R.string.mundo_deportivo, R.mipmap.mundo_deportivo, "http://www.mundodeportivo.com/feed/rss/futbol/atletico-madrid"});

        NEWSPAPERS_MAP = Collections.unmodifiableMap(aMap);
    }
}
