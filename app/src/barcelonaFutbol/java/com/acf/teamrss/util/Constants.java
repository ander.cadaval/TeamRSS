package com.acf.teamrss.util;

import com.acf.teamrss.R;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class Constants {
    public static final Map<Integer, Object[]> NEWSPAPERS_MAP;
    static {
        Map<Integer, Object[]> aMap = new LinkedHashMap<>();

        //aMap.put(R.string.barcelona, new Object[] {R.string.barcelona, R.mipmap.barcelona, "http://www.fcbarcelona.es/futbol/listado/noticias.rss"});
        aMap.put(R.string.fcbn, new Object[] {R.string.fcbn, R.mipmap.fcbn, "http://www.fcbarcelonanoticias.com/rss/todas-las-noticias-de-fcbn-001.xml"});
        aMap.put(R.string.mundo_deportivo, new Object[] {R.string.mundo_deportivo, R.mipmap.mundo_deportivo, "http://www.mundodeportivo.com/feed/rss/futbol/fc-barcelona"});
        aMap.put(R.string.sport, new Object[] {R.string.sport, R.mipmap.sport, "http://www.sport.es/es/rss/barca/rss.xml"});
        aMap.put(R.string.marca, new Object[] {R.string.marca, R.mipmap.marca, "http://estaticos.marca.com/rss/futbol_equipos_barcelona.xml"});
        aMap.put(R.string.as, new Object[] {R.string.as, R.mipmap.as, "http://masdeporte.as.com/tag/rss/fc_barcelona/a"});
        //aMap.put(R.string.huffingtonpost, new Object[] {R.string.huffingtonpost, R.mipmap.huffingtonpost, "http://www.huffingtonpost.es/news/fc-barcelona/feed//"});

        NEWSPAPERS_MAP = Collections.unmodifiableMap(aMap);
    }
}
