package com.acf.teamrss.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acf.teamrss.R;
import com.acf.teamrss.models.Newspaper;

import java.util.List;

public class NewspaperViewHolderAdapter extends RecyclerView.Adapter<NewspaperViewHolderAdapter.NewspaperViewHolder> implements View.OnClickListener {

    /*
     * Properties
     */
    private List<Newspaper> newspapers;
    private View.OnClickListener listener;
    public static int selectedItem = 0;

    /*
     * Constructors
     */
    public NewspaperViewHolderAdapter(List<Newspaper> newspapers) {
        this.newspapers = newspapers;
    }

    /*
     * GET/SET methods
     */

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    /*
     * RecyclerView.Adapter methods
     */

    @Override
    public NewspaperViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_newspaper, viewGroup, false);

        itemView.setOnClickListener(this);

        return new NewspaperViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewspaperViewHolder holder, int position) {
        Newspaper newspaper = this.newspapers.get(position);

        holder.bindNewspaper(newspaper);

        if(position == selectedItem)
        {
            holder.rlRowNewspaper.setSelected(true);
        }
        else
        {
            holder.rlRowNewspaper.setSelected(false);
        }
    }

    @Override
    public int getItemCount() { return this.newspapers.size(); }

    /*
     * View.OnClickListener methods
     */

    @Override
    public void onClick(View view) {
        if(this.listener != null) {
            this.listener.onClick(view);
        }
    }

    /*
     * NewspaperViewHolder class
     */

    public static class NewspaperViewHolder extends RecyclerView.ViewHolder {

        /*
         * Properties
         */

        private RelativeLayout rlRowNewspaper;
        private TextView newspaperLink;
        private ImageView image;
        private TextView name;

        /*
         * Constructors
         */

        public NewspaperViewHolder(View itemView) {
            super(itemView);

            this.rlRowNewspaper = (RelativeLayout) itemView.findViewById((R.id.rlRowNewspaper));
            this.newspaperLink = (TextView) itemView.findViewById(R.id.lblNewspaperLink);
            this.image = (ImageView) itemView.findViewById(R.id.imgNewspaperImage);
            this.name = (TextView) itemView.findViewById(R.id.lblNewspaperName);
        }

        /*
         * Public methods
         */

        public void bindNewspaper(Newspaper newspaper) {
            this.rlRowNewspaper.setSelected(false);
            this.newspaperLink.setText(newspaper.getNewspaperLink());
            this.image.setImageResource(newspaper.getImage());
            this.name.setText(newspaper.getName());

            this.image.setContentDescription(this.name.getText());
        }
    }
}
