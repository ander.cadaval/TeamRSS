package com.acf.teamrss.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acf.teamrss.R;
import com.acf.teamrss.application.AppController;
import com.acf.teamrss.models.New;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

public class NewViewHolderAdapter extends RecyclerView.Adapter<NewViewHolderAdapter.NewViewHolder> implements View.OnClickListener {

    /*
     * Properties
     */
    private List<New> news;
    private View.OnClickListener listener;

    /*
     * Constructors
     */

    public NewViewHolderAdapter(List<New> news) {
        this.news = news;
    }

    /*
     * GET/SET methods
     */

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    /*
     * RecyclerView.Adapter methods
     */

    @Override
    public NewViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_new, viewGroup, false);

        itemView.setOnClickListener(this);

        return new NewViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewViewHolder holder, int position) {
        New newObject = this.news.get(position);

        holder.bindNew(newObject);
    }

    @Override
    public int getItemCount() {
        return this.news.size();
    }

    /*
     * View.OnClickListener methods
     */

    @Override
    public void onClick(View view) {
        if(this.listener != null) {
            this.listener.onClick(view);
        }
    }

    /*
     * NewViewHolder class
     */

    public static class NewViewHolder extends RecyclerView.ViewHolder {

        /*
         * Properties
         */

        private TextView title;
        private TextView link;
        private TextView description;
        private TextView date;
        private NetworkImageView image;

        private ImageLoader imageLoader;

        /*
         * Constructors
         */

        public NewViewHolder(View itemView) {
            super(itemView);

            imageLoader = AppController.getInstance().getImageLoader();

            this.title = (TextView) itemView.findViewById(R.id.lblNewTitle);
            this.link = (TextView) itemView.findViewById(R.id.lblNewLink);
            this.description = (TextView) itemView.findViewById(R.id.lblNewDescription);
            this.date = (TextView) itemView.findViewById(R.id.lblNewDate);
            this.image = (NetworkImageView) itemView.findViewById(R.id.imgNewImage);
        }

        /*
         * Public methods
         */

        public void bindNew(New newObject) {
            this.title.setText(newObject.getTitle());
            this.link.setText(newObject.getLink());
            this.date.setText(newObject.getDate());

            if (!"".equals(newObject.getDescription())) {
                this.description.setText(newObject.getDescription());
            } else {
                this.description.setVisibility(View.GONE);
            }

            if (!"".equals(newObject.getImage())) {
                this.image.setImageUrl(newObject.getImage(), this.imageLoader);
                this.image.setContentDescription(title.getText());
                this.image.setVisibility(View.VISIBLE);
            } else {
                this.image.setVisibility(View.GONE);
            }
        }
    }
}