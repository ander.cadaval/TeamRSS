package com.acf.teamrss.interactors;

import com.acf.teamrss.application.AppController;
import com.acf.teamrss.listeners.OnListNewsFinishedListener;
import com.acf.teamrss.models.New;
import com.acf.teamrss.util.Logger;
import com.acf.teamrss.util.RSSUtil;
import com.acf.teamrss.volley.XmlRequest;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsInteractorImpl implements NewsInteractor {

	/*
	 * NewsInteractor methods
	 */
	
	@Override
	public void loadNews(String newspaperTitle, String newspaperUrl, final OnListNewsFinishedListener listener) throws AuthFailureError {
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(newspaperUrl);
        if(entry != null){
            try {
                InputStream inputStream = new ByteArrayInputStream(entry.data);

                List<New> news = RSSUtil.parse(inputStream);

                listener.onLoadNewsFinished(news);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Map<String, String> headers = new HashMap<>();
            headers.put("User-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64");

            XmlRequest xmlReq = new XmlRequest<>(
                    newspaperUrl,
                    headers,
                    new Response.Listener<List<New>>() {
                        @Override
                        public void onResponse(List<New> response) {
                            listener.onLoadNewsFinished(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                           //Logger.e("NewsInteractorImpl", "Error: " + error.getLocalizedMessage());
                        }
                    }
            );

            AppController.getInstance().addToRequestQueue(xmlReq, "news_req_" + newspaperTitle.replace(" ", "_"));
        }

	}

	@Override
	public void selectNew(String newspaperTitle, String newLink, OnListNewsFinishedListener listener) {
        HashMap<String, String> extras = new HashMap<>();

        if (!"".equals(newLink)) {
			extras.put("newspaperTitle", newspaperTitle);
            extras.put("newLink", newLink);
        }

		listener.onSelectNewFinished(extras);
	}
}