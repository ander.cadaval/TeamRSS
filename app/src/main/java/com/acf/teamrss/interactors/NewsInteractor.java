package com.acf.teamrss.interactors;

import com.acf.teamrss.listeners.OnListNewsFinishedListener;
import com.android.volley.AuthFailureError;

public interface NewsInteractor {

	void loadNews(String newspaperTitle, String newspaperUrl, OnListNewsFinishedListener listener) throws AuthFailureError;
	
	void selectNew(String newspaperTitle, String newLink, OnListNewsFinishedListener listener);
}
