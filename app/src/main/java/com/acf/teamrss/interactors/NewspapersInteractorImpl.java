package com.acf.teamrss.interactors;

import com.acf.teamrss.listeners.OnListNewspapersFinishedListener;
import com.acf.teamrss.models.Newspaper;
import com.acf.teamrss.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class NewspapersInteractorImpl implements NewspapersInteractor {
	
	/*
	 * NewspapersInteractor methods
	 */
	
	@Override
	public void loadNewspapers(final OnListNewspapersFinishedListener listener) {
		Newspaper defaultNewspaper = null;
		Newspaper newspaper;
		List<Newspaper> newspapers = new ArrayList<>();

		for (int key : Constants.NEWSPAPERS_MAP.keySet()) {
			newspaper = new Newspaper();

			newspaper.setNewspaperLink(Constants.NEWSPAPERS_MAP.get(key)[2].toString());
			newspaper.setImage((int) Constants.NEWSPAPERS_MAP.get(key)[1]);
			newspaper.setName((int) Constants.NEWSPAPERS_MAP.get(key)[0]);

			newspapers.add(newspaper);

			if (defaultNewspaper == null) {
				defaultNewspaper = newspaper;
			}
		}

		listener.onLoadNewspapersFinished(newspapers, defaultNewspaper);
	}
}