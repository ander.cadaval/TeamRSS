package com.acf.teamrss.interactors;

import com.acf.teamrss.listeners.OnListNewspapersFinishedListener;

public interface NewspapersInteractor {

	void loadNewspapers(OnListNewspapersFinishedListener listener);
}
