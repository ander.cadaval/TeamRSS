package com.acf.teamrss.interactors;

import com.acf.teamrss.listeners.OnWelcomeFinishedListener;

public interface WelcomeInteractor {

	void checkConnection(OnWelcomeFinishedListener listener);
}
