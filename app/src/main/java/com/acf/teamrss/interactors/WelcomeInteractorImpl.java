package com.acf.teamrss.interactors;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.acf.teamrss.listeners.OnWelcomeFinishedListener;

public class WelcomeInteractorImpl implements WelcomeInteractor {

	/*
	 * Properties
	 */

	private Context context;

	/*
	 * Constructors
	 */

	public WelcomeInteractorImpl(Context context) {
		super();

		this.context = context;
	}

	/*
	 * WelcomeInteractor methods
	 */
	
	@Override
	public void checkConnection(OnWelcomeFinishedListener listener) {

		ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (activeNetwork != null) {
            //For check connection
            boolean isConnected = activeNetwork.isConnectedOrConnecting();

            //For 3G check
            boolean is3g = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;

            //For WiFi Check
            boolean isWifi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

            if (isConnected && (is3g || isWifi)) {
                listener.onConnected();
            } else {
                listener.onNotConnected();
            }
        } else {
            listener.onNotConnected();
        }
	}
}
