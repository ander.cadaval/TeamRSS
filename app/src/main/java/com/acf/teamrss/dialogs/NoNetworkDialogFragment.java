package com.acf.teamrss.dialogs;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.acf.teamrss.R;
import com.acf.teamrss.listeners.NoNetworkDialogListener;

//@TargetApi(14)
public class NoNetworkDialogFragment extends DialogFragment implements OnClickListener {

	/*
	 * Properties
	 */

	private NoNetworkDialogListener listener;

	/*
	 * Constructors
	 */

	public static NoNetworkDialogFragment newInstance(){
		return new NoNetworkDialogFragment();
	}

	public NoNetworkDialogFragment() {
		//Empty constructor
	}

	/*
     * DialgoFragment methods
     */

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		this.listener = (NoNetworkDialogListener) this.getActivity();
	}
	
	@NonNull
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity(), R.style.MyAlertDialogStyle);

        builder.setTitle(R.string.no_network_active_title);
		builder.setMessage(R.string.no_network_active_message);
		
		builder.setPositiveButton(R.string.wifi, this);
		builder.setNeutralButton(android.R.string.cancel, this);
		builder.setNegativeButton(R.string.data, this);

        return builder.create();
    }

	/*
	 * OnClickListener methods
	 */
	
	@Override
	public void onClick(DialogInterface dialog, int id) {
		switch (id) {
			case DialogInterface.BUTTON_POSITIVE:
				this.listener.onNoNetworkDialogPositiveClick(this);
				
				break;
			case DialogInterface.BUTTON_NEUTRAL:
				this.listener.onNoNetworkDialogNeutralClick(this);
				
				break;
			case DialogInterface.BUTTON_NEGATIVE:
				this.listener.onNoNetworkDialogNegativeClick(this);
				
				break;
			default:
				break;
		}
	}
}
