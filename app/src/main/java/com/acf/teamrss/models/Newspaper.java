package com.acf.teamrss.models;

public class Newspaper {

    /*
     * Properties
     */

    private String newspaperLink;
    private int image;
    private int name;

    /*
     * Constructors
     */

    public Newspaper() {
        this.newspaperLink = "";
        this.image = 0;
        this.name = 0;
    }

    /*
     * GET/SET methods
     */

    public String getNewspaperLink() { return newspaperLink; }

    public void setNewspaperLink(String newspaperLink) { this.newspaperLink = newspaperLink; }

    public int getImage() { return this.image; }

    public void setImage(int image) { this.image = image; }

    public int getName() { return this.name; }

    public void setName(int name) { this.name = name; }
}
