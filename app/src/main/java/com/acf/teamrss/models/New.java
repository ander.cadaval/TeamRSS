package com.acf.teamrss.models;

public class New {

    /*
     * Properties
     */

    private String title;
    private String link;
    private String description;
    private String date;
    private String image;
    //private String content;

    /*
     * Constructors
     */

    public New() {
        this.title = "";
        this.link = "";
        this.description = "";
        this.date = "";
        this.image = "";
        //this.content = "";
    }

    /*
     * GET/SET methods
     */

    public String getTitle() { return this.title; }

    public void setTitle(String title) { this.title = title; }

    public String getLink() {
        return this.link;
    }

    public void setLink(String description) {
        this.link = description;
    }

    public String getDescription() { return this.description; }

    public void setDescription(String description) { this.description = description; }

    public String getDate() { return this.date; }

    public void setDate(String date) { this.date = date; }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    /*
    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    */
}
