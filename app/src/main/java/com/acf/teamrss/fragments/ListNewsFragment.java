package com.acf.teamrss.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acf.teamrss.R;
import com.acf.teamrss.activities.NewActivity;
import com.acf.teamrss.adapters.NewViewHolderAdapter;
import com.acf.teamrss.application.AppController;
import com.acf.teamrss.models.New;
import com.acf.teamrss.presenters.ListNewsPresenter;
import com.acf.teamrss.presenters.ListNewsPresenterImpl;
import com.acf.teamrss.util.AnalyticsUtil;
import com.acf.teamrss.util.Logger;
import com.acf.teamrss.util.Util;
import com.acf.teamrss.views.ListNewsView;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.List;

public class ListNewsFragment extends Fragment implements ListNewsView, View.OnClickListener, Runnable, SwipeRefreshLayout.OnRefreshListener {

	/*
	 * Properties
	 */

	private AppCompatActivity activity;
	private ListNewsPresenter presenter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recView;

	public static final String IS_DEFAULT_NEWSPAPER = "isDefaultNewspaper";
    public static final String ARGUMENT_NEWSPAPER_TITLE = "newspaperTitle";
    public static final String ARGUMENT_URL = "url";

	/*
	 * Constructors
	 */

	public static ListNewsFragment newInstance(Bundle arguments){
		ListNewsFragment f = new ListNewsFragment();
		if(arguments != null){
			f.setArguments(arguments);
		}
		return f;
	}

	/*
	 * Fragment methods
	 */

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		View view = inflater.inflate(R.layout.fragment_list_news, container, false);

		if (view != null) {
            this.swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
			this.recView = (RecyclerView) view.findViewById(R.id.newsRecView);
		}

		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		this.activity = (AppCompatActivity) this.getActivity();

        // [START shared_tracker]
        // Obtain the shared Tracker instance.
        Tracker tracker = AnalyticsUtil.getTracker(this.activity.getApplication());
        // [END shared_tracker]

		this.presenter = new ListNewsPresenterImpl(this);

        this.swipeRefreshLayout.setOnRefreshListener(this);

        //Can add more than one color comma separated
		this.swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayoutColor);

        this.recView.setLayoutManager(
				new LinearLayoutManager(this.activity, LinearLayoutManager.VERTICAL, false));

		/**
		 * Showing Swipe Refresh animation on activity create
		 * As animation won't start on onCreate, post runnable is used
		 */
		this.swipeRefreshLayout.post(this);

		if (this.activity.getSupportActionBar() != null) {
			if (this.getArguments() != null) {
				this.activity.getSupportActionBar().setTitle(this.getArguments().getString(ARGUMENT_NEWSPAPER_TITLE));

                //Add Screen name
                if (!this.getArguments().getBoolean(IS_DEFAULT_NEWSPAPER)) {
                    AnalyticsUtil.setScreenName(tracker,
                            this.getClass().getSimpleName(),
                            this.activity.getSupportActionBar().getTitle() + " - List");
                }
			}
		}
	}

	/*
	 * ListNewsView methods
	 */
	
	@Override
	public void setNews(List<New> news) {
		final NewViewHolderAdapter adapter = new NewViewHolderAdapter(news);

        adapter.setOnClickListener(this);

        this.recView.setAdapter(adapter);
        /*
        this.recView.addItemDecoration(
                new DividerItemDecoration(this,DividerItemDecoration.VERTICAL_LIST));
        */

        this.recView.setItemAnimator(new DefaultItemAnimator());
        
        // stopping swipe refresh
        this.swipeRefreshLayout.setRefreshing(false);
	}

	@Override
	public void navigateToNew(HashMap<String, String> extras) {
		Util.openLayout(this.activity, NewActivity.class, extras);
		
		//this.activity.finish();
	}
	
	/*
	 * OnItemClickListener methods
	 */
	
	@Override
	public void onClick(View view) {
		TextView txtNewLink = (TextView) view.findViewById(R.id.lblNewLink);

        if (this.getArguments() != null) {
            this.presenter.onItemClicked(this.getArguments().getString(ARGUMENT_NEWSPAPER_TITLE), txtNewLink.getText().toString());
        }
    }

    /*
     * Runnable
     */

    @Override
    public void run() {
        // showing refresh animation before making http call
        this.swipeRefreshLayout.setRefreshing(true);

        if (this.getArguments() != null) {
            this.presenter.onResume(this.getArguments().getString(ARGUMENT_NEWSPAPER_TITLE), this.getArguments().getString(ARGUMENT_URL));
        }
    }

    /*
     * SwipeRefreshLayout.OnRefreshListener methods
     */

    @Override
    public void onRefresh() {
        AppController.getInstance().getRequestQueue().getCache().remove(this.getArguments().getString(ARGUMENT_URL));
		this.swipeRefreshLayout.post(this);
    }
}
