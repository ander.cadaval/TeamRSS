package com.acf.teamrss.fragments;

import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.acf.teamrss.R;

public class NewFragment extends Fragment {

    /*
	 * Properties
	 */

    private AppCompatActivity activity;
    private WebView webView;
	private ProgressBar progressBar;

	/*
	 * Constructors
	 */

	public static NewFragment newInstance(Bundle arguments){
		NewFragment f = new NewFragment();
		if(arguments != null){
			f.setArguments(arguments);
		}
		return f;
	}

	public NewFragment() {
		//Empty constructor
	}

	/*
	 * Fragment methods
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        this.activity = (AppCompatActivity) this.getActivity();

        View view = inflater.inflate(R.layout.fragment_new, container, false);

        if (view != null) {
            this.webView = (WebView) view.findViewById(R.id.webView);
			this.progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            this.progressBar.setMax(100);
			this.progressBar.getProgressDrawable().setColorFilter(ContextCompat.getColor(this.activity.getApplicationContext(), R.color.progressBar), PorterDuff.Mode.SRC_IN);

            this.webView.setWebViewClient(new WebViewClient());
			this.webView.setWebChromeClient(new WebChromeClient() {

				public void onProgressChanged(WebView view, int progress)
				{
                    if(progress < 100 && progressBar.getVisibility() == ProgressBar.GONE){
						progressBar.setVisibility(ProgressBar.VISIBLE);
					}
					progressBar.setProgress(progress);
					if(progress == 100) {
						progressBar.setVisibility(ProgressBar.GONE);
					}
				}
			});
            this.webView.getSettings().setJavaScriptEnabled(true);
            //this.webView.getSettings().setBuiltInZoomControls(true);
            //this.webView.getSettings().setDisplayZoomControls(false);
        }

        return view;
    }
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		String newspaperTitle;
		String newLink = "";
		Bundle extras = activity.getIntent().getExtras();

		if (extras != null) {
			newspaperTitle = extras.getString("newspaperTitle");
			newLink = extras.getString("newLink");

			if (activity.getSupportActionBar() != null) {
				activity.getSupportActionBar().setTitle(newspaperTitle);
			}
		}

		this.webView.loadUrl(newLink);
	}
}
