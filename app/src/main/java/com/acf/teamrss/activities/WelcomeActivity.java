package com.acf.teamrss.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;

import com.acf.teamrss.application.AppController;
import com.acf.teamrss.dialogs.NoNetworkDialogFragment;
import com.acf.teamrss.listeners.NoNetworkDialogListener;
import com.acf.teamrss.presenters.WelcomePresenter;
import com.acf.teamrss.presenters.WelcomePresenterImpl;
import com.acf.teamrss.util.Util;
import com.acf.teamrss.views.WelcomeView;


public class WelcomeActivity extends AppCompatActivity implements WelcomeView, NoNetworkDialogListener {

    /*
	 * Properties
	 */

    private WelcomePresenter presenter;

    /*
     * Activity methods
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.presenter = new WelcomePresenterImpl(this, this.getApplicationContext());

        AppController.getInstance().getRequestQueue().getCache().clear();
    }

    @Override
    public void onResume() {
        super.onResume();

        this.presenter.checkConnection();
    }

    /*
	 * WelcomeView methods
	 */

    @Override
    public void navigateToListNewspapers() {
        Util.openLayout(WelcomeActivity.this, MainActivity.class);

        finish();
    }

    @Override
    public void openNotNetworkDialog() {
        NoNetworkDialogFragment dialog = NoNetworkDialogFragment.newInstance();

        dialog.show(this.getSupportFragmentManager(), "noNetworkDialog");
    }

	/*
	 * NoNetworkDialogListener methods
	 */

    @Override
    public void onNoNetworkDialogPositiveClick(NoNetworkDialogFragment dialog) {
        this.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    @Override
    public void onNoNetworkDialogNeutralClick(NoNetworkDialogFragment dialog) {
        dialog.dismiss();

        this.finish();
    }

    @Override
    public void onNoNetworkDialogNegativeClick(NoNetworkDialogFragment dialog) {
        this.startActivity(new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS));
    }
}