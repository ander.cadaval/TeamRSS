package com.acf.teamrss.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.acf.teamrss.R;
import com.acf.teamrss.fragments.NewFragment;
import com.acf.teamrss.util.AnalyticsUtil;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.Tracker;

public class NewActivity extends AppCompatActivity implements View.OnClickListener {

	/*
	 * Properties
	 */

	private AdView adView;
	private Tracker tracker;

	/*
	 * Activity methods
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_new);

		// Obtain the shared Tracker instance.
		this.tracker = AnalyticsUtil.getTracker(getApplication());

		//if (android.os.Build.VERSION.SDK_INT > Constants.APP_MIN_VERSION) {
			this.adView = (AdView) this.findViewById(R.id.adView);

            AdRequest adRequest = new AdRequest.Builder().build();
            //AdRequest adRequest = new AdRequest.Builder().addTestDevice("E5BDB4422419B850298A3D20052F682C").build();

            this.adView.loadAd(adRequest);

            this.adView.setAdListener(new AdListener() {

                @Override
                public void onAdLoaded() { adView.setVisibility(View.VISIBLE); }

                @Override
                public void onAdFailedToLoad(int errorCode) {}
            });
		//}

        Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar);
		this.setSupportActionBar(toolbar);

        if (this.getSupportActionBar() != null) {
		    this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		    this.getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

		toolbar.setNavigationOnClickListener(this);

		if (savedInstanceState == null) {
			this.getSupportFragmentManager().beginTransaction()
					.add(R.id.content_frame, NewFragment.newInstance(null)).commit();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		//if (this.adView != null && android.os.Build.VERSION.SDK_INT > Constants.APP_MIN_VERSION) {
			this.adView.resume();
		//}

        Bundle extras = this.getIntent().getExtras();
        if (extras != null) {
            //Add Screen name
			AnalyticsUtil.setScreenName(this.tracker,
					this.getClass().getSimpleName(),
					extras.getString("newspaperTitle") + " - New");
        }
	}

	@Override
	protected void onPause() {
		super.onPause();

		//if (this.adView != null && android.os.Build.VERSION.SDK_INT > Constants.APP_MIN_VERSION) {
			this.adView.pause();
		//}
	}

	/*
	 * OnClickListener methods
	 */

	@Override
	public void onClick(View v) {
		this.finish();
	}
}
