package com.acf.teamrss.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.acf.teamrss.R;
import com.acf.teamrss.adapters.DividerItemDecoration;
import com.acf.teamrss.adapters.NewspaperViewHolderAdapter;
import com.acf.teamrss.fragments.ListNewsFragment;
import com.acf.teamrss.models.Newspaper;
import com.acf.teamrss.presenters.MainActivityPresenter;
import com.acf.teamrss.presenters.MainActivityPresenterImpl;
import com.acf.teamrss.views.MainActivityView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.List;

public class MainActivity extends AppCompatActivity implements /*NavigationView.OnNavigationItemSelectedListener,*/ View.OnClickListener, MainActivityView {

    /*
	 * Properties
	 */

    private Toolbar toolbar;
	private DrawerLayout drawerLayout;
	//private NavigationView navView;
	private RecyclerView recView;
    private InterstitialAd interstitialAd;
    private AdView adView;

	private MainActivityPresenter presenter;

	/*
	 * Activity methods
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_main);

        //if (android.os.Build.VERSION.SDK_INT > Constants.APP_MIN_VERSION) {
            // Create the BannerAd
			this.adView = (AdView) this.findViewById(R.id.adView);
			
            AdRequest adRequest = new AdRequest.Builder().build();
            //AdRequest adRequest = new AdRequest.Builder().addTestDevice("E5BDB4422419B850298A3D20052F682C").build();
            this.adView.loadAd(adRequest);

            this.adView.setAdListener(new AdListener() {

                @Override
                public void onAdLoaded() {
                    adView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                }
            });

            this.interstitialAd = new InterstitialAd(this);
            this.interstitialAd.setAdUnitId(this.getString(R.string.intersticial_ad_unit_id));

            this.interstitialAd.loadAd(adRequest);
        //}

        this.toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        this.setSupportActionBar(this.toolbar);

		this.drawerLayout = (DrawerLayout) this.findViewById(R.id.drawer_layout);
		//this.navView = (NavigationView) this.findViewById(R.id.nav_view);
		this.recView = (RecyclerView) this.findViewById(R.id.newspapersRecView);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, this.drawerLayout, this.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.syncState();

        this.drawerLayout.setDrawerListener(toggle);

        //this.navView.setNavigationItemSelectedListener(this);

        this.presenter = new MainActivityPresenterImpl(this);

        this.presenter.loadNewspapers();
	}


    @Override
    protected void onStart() {
        super.onStart();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, this.drawerLayout, this.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.syncState();

        this.drawerLayout.setDrawerListener(toggle);

        //this.navView.setNavigationItemSelectedListener(this);

        this.presenter = new MainActivityPresenterImpl(this);

        this.presenter.loadNewspapers();
    }

	@Override
	public void onResume() {
		super.onResume();

        //if (this.adView != null && android.os.Build.VERSION.SDK_INT > Constants.APP_MIN_VERSION) {
            this.adView.resume();
        //}
	}

    @Override
    protected void onPause() {
        super.onPause();

        //if (this.adView != null && android.os.Build.VERSION.SDK_INT > Constants.APP_MIN_VERSION) {
            this.adView.pause();
        //}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                this.drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onBackPressed() {
		if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}

	/*
	 * OnNavigationItemSelectedListener methods
	 */
    /*
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
        Bundle arguments = new Bundle();
        if (item.getItemId() != 0) {
            arguments.putString(ListNewsFragment.ARGUMENT_URL, Constants.NEWSPAPERS_MAP.get(item.getItemId())[2].toString());
            ListNewsFragment listNewsFragment = ListNewsFragment.newInstance(arguments);

            this.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, listNewsFragment)
                    .commit();

            item.setChecked(true);

            if (this.getSupportActionBar() != null) {
                this.getSupportActionBar().setTitle(item.getTitle());
            }
        }

        this.drawerLayout.closeDrawer(GravityCompat.START);
		return true;
	}
    */
    /*
	 * OnItemClickListener methods
	 */

    @Override
    public void onClick(View view) {
        if (this.interstitialAd.isLoaded()) {
            this.interstitialAd.show();
        }

        Bundle arguments = new Bundle();
        TextView txtNewspaperLink = (TextView) view.findViewById(R.id.lblNewspaperLink);
        TextView txtNewspaperName = (TextView) view.findViewById(R.id.lblNewspaperName);

        if (!"".equals(txtNewspaperLink.getText().toString())) {
            arguments.putBoolean(ListNewsFragment.IS_DEFAULT_NEWSPAPER, false);
            arguments.putString(ListNewsFragment.ARGUMENT_NEWSPAPER_TITLE, txtNewspaperName.getText().toString());
            arguments.putString(ListNewsFragment.ARGUMENT_URL, txtNewspaperLink.getText().toString());
            ListNewsFragment listNewsFragment = ListNewsFragment.newInstance(arguments);

            this.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, listNewsFragment)
                    .commit();
        }

        NewspaperViewHolderAdapter.selectedItem = this.recView.getChildAdapterPosition(view);

        this.drawerLayout.closeDrawer(GravityCompat.START);
        this.recView.getAdapter().notifyDataSetChanged();
    }

	/*
	 * ListNewspapersView methods
	 */

	@Override
	public void setNewspapers(List<Newspaper> newsPapers, Newspaper defaultNewspaper) {
        final NewspaperViewHolderAdapter adapter = new NewspaperViewHolderAdapter(newsPapers);

        adapter.setOnClickListener(this);

        this.recView.setAdapter(adapter);
        this.recView.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        this.recView.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        this.recView.setItemAnimator(new DefaultItemAnimator());

        if (this.getSupportFragmentManager().getFragments() == null) {
            this.selectDefaultNewspaper(defaultNewspaper);
        } else {
            Fragment fragment = this.getSupportFragmentManager().getFragments().get(0);
            if (fragment.getArguments().containsKey(ListNewsFragment.ARGUMENT_NEWSPAPER_TITLE)) {
                if ("".equals(fragment.getArguments().get(ListNewsFragment.ARGUMENT_NEWSPAPER_TITLE))) {
                    this.selectDefaultNewspaper(defaultNewspaper);
                }
            } else {
                this.selectDefaultNewspaper(defaultNewspaper);
            }
        }
	}

    /*
     * Own methods
     */

    private void selectDefaultNewspaper(Newspaper defaultNewspaper) {
        Bundle arguments = new Bundle();

        if (defaultNewspaper != null) {
            arguments.putBoolean(ListNewsFragment.IS_DEFAULT_NEWSPAPER, true);
            arguments.putString(ListNewsFragment.ARGUMENT_NEWSPAPER_TITLE, this.getString(defaultNewspaper.getName()));
            arguments.putString(ListNewsFragment.ARGUMENT_URL, defaultNewspaper.getNewspaperLink());
            ListNewsFragment listNewsFragment = ListNewsFragment.newInstance(arguments);

            this.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, listNewsFragment)
                    .commit();
        }
    }
}
