package com.acf.teamrss.util;

import android.content.Context;
import android.content.Intent;

import java.util.HashMap;

public class Util {
    /**
     * Open activity layout
     *
     * @param context Curretnt context
     * @param cls Activity that will be displayed
     */
    public static void openLayout(Context context, Class<?> cls) {
        openLayout(context, cls, null);
    }

    /**
     * Open activity layout
     *
     * @param context Curretnt context
     * @param cls Activity that will be displayed
     * @param extras Values to be passed from current activity to new activity
     */
    public static void openLayout(Context context, Class<?> cls, HashMap<String,String> extras) {
        Intent intent = new Intent(context, cls);

        if (extras != null) {
            for(String key : extras.keySet()) {
                intent.putExtra(key, extras.get(key));
            }
        }

        context.startActivity(intent);
    }
}
