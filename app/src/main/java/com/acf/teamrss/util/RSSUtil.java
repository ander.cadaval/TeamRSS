package com.acf.teamrss.util;

import android.text.Html;

import com.acf.teamrss.models.New;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class RSSUtil {
    /*
    public static List<New> parse(InputStream inputStream) throws Exception {
        List<New> news = new ArrayList<>();

        SimpleDateFormat outDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        // Reading the feed
        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feed = input.build(new XmlReader(inputStream));
        List entries = feed.getEntries();
        Iterator itEntries = entries.iterator();

        New newObject;
        while (itEntries.hasNext()) {
            SyndEntry entry = (SyndEntry) itEntries.next();

            newObject = new New();
            newObject.setTitle(entry.getTitle());
            newObject.setLink(entry.getLink());
            newObject.setDescription(entry.getDescription().getValue());
            newObject.setDate(outDateFormat.format(entry.getPublishedDate()));
            if (entry.getEnclosures().size() > 0) {
                SyndEnclosure enc = (SyndEnclosure) entry.getEnclosures().get(0);
                newObject.setImage(enc.getUrl());
            }

            news.add(newObject);
        }

        return news;
    }
    */
    public static List<New> parse(InputStream inputStream) throws Exception {
        List<New> news = null;

        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(inputStream, null);
            parser.nextTag();
            news = readRss(parser);
        } catch (Exception e) {
            //Logger.e("RSSUtil", "Error: " + error.getLocalizedMessage());
        }

        return news;
    }

    private static List<New> readRss(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<New> items = new ArrayList<>();
        String name;

        parser.require(XmlPullParser.START_TAG, null, "rss");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }

            name = parser.getName();
            if ("channel".equals(name)) {
                items.addAll(readChannel(parser));
            } else {
                skip(parser);
            }
        }
        return items;
    }

    private static List<New> readChannel(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<New> items = new ArrayList<>();
        New newObject;
        String name;

        parser.require(XmlPullParser.START_TAG, null, "channel");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            name = parser.getName();
            if ("item".equals(name)) {
                newObject = readItem(parser);
                if (newObject != null) {
                    items.add(newObject);
                }
            } else {
                skip(parser);
            }
        }
        return items;
    }

    private static New readItem(XmlPullParser parser) throws XmlPullParserException, IOException {
        New newObject = new New();
        String name;
        String prefix;
        String pubDateString;
        GregorianCalendar pubDate;

        SimpleDateFormat inDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
        SimpleDateFormat outDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());


        parser.require(XmlPullParser.START_TAG, null, "item");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            name = parser.getName();
            prefix = parser.getPrefix();
            if ("title".equals(name) && prefix == null) {
                newObject.setTitle(readField(parser, name));
            } else if ("link".equals(name)) {
                newObject.setLink(readField(parser, name));
            } else if ("description".equals(name)) {
                if ("".equals(newObject.getDescription())) {
                    newObject.setDescription(readField(parser, name));
                } else if (!"".equals(newObject.getDescription()) && "media".equals(prefix)) {
                    newObject.setDescription(readField(parser, name));
                } else {
                    skip(parser);
                }
            } else if ("pubDate".equals(name)) {
                pubDate = new GregorianCalendar();

                try {
                    pubDateString = readField(parser, name);
                    pubDate.setTime(inDateFormat.parse(pubDateString));
                } catch (ParseException e) {
                    //Logger.e("RSSUtil", "pubDate parse exception: " + e.getLocalizedMessage());
                }

                newObject.setDate(outDateFormat.format(pubDate.getTime()));
            } else if ("enclosure".equals(name)) {
                newObject.setImage(readAttachment(parser));
            } else if ("thumbnail".equals(name)) {
                newObject.setImage(readAttachment(parser));
            //} else if ("content".equals(name) && "media".equals(prefix)) {
                //newObject.setImage(readAttachment(parser));
            //} else if ("encoded".equals(name) && "content".equals(prefix)) {
                //newObject.setContent(readField(parser, name));
            } else {
                skip(parser);
            }
        }
        parser.require(XmlPullParser.END_TAG, null, "item");

        if ("".equals(newObject.getTitle())) {
            newObject = null;
        }
        return newObject;
    }

    private static String readField(XmlPullParser parser, String field) throws IOException, XmlPullParserException {
        String element;

        parser.require(XmlPullParser.START_TAG, null, field);
        element = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, field);

        return element;
    }

    private static String readAttachment(XmlPullParser parser) throws IOException, XmlPullParserException {
        String element;

        element = parser.getAttributeValue(null, "url");
        parser.nextTag();

        return element;
    }

    private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        String html;

        if (parser.next() == XmlPullParser.TEXT) {
            html = parser.getText();
            html = html.replaceAll("<(.*?)\\>", " ");//Removes all items in brackets
            html = html.replaceAll("<(.*?)\\\n", " ");//Must be undeneath
            html = html.replaceFirst("(.*?)\\>", " ");//Removes any connected item to the last bracket
            html = html.replaceAll("&nbsp;", " ");
            html = html.replaceAll("&amp;", " ");

            result = Html.fromHtml(html).toString();
            parser.nextTag();
        }
        return result;
    }

    private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
