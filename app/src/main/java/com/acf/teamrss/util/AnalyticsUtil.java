package com.acf.teamrss.util;

import android.content.Context;

import com.acf.teamrss.application.AppController;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class AnalyticsUtil {

    public static Tracker getTracker(Context context) {
        AppController application = (AppController) context;
        return application.getDefaultTracker();
    }

    public static void setScreenName(Tracker tracker, String className, String title) {
        tracker.setScreenName(title);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
