package com.acf.teamrss.listeners;

public interface OnWelcomeFinishedListener {

	void onConnected();

	void onNotConnected();
}
