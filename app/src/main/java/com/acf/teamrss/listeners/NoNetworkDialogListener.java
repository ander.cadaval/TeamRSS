package com.acf.teamrss.listeners;

import com.acf.teamrss.dialogs.NoNetworkDialogFragment;

public interface NoNetworkDialogListener {

	void onNoNetworkDialogPositiveClick(NoNetworkDialogFragment dialog);
	
	void onNoNetworkDialogNeutralClick(NoNetworkDialogFragment dialog);

	void onNoNetworkDialogNegativeClick(NoNetworkDialogFragment dialog);

}
