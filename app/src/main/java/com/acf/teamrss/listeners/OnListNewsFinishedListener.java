package com.acf.teamrss.listeners;

import com.acf.teamrss.models.New;

import java.util.HashMap;
import java.util.List;

public interface OnListNewsFinishedListener {

	void onLoadNewsFinished(List<New> news);
	
	void onSelectNewFinished(HashMap<String, String> extras);
}
