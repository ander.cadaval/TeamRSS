package com.acf.teamrss.listeners;

import com.acf.teamrss.models.Newspaper;

import java.util.List;

public interface OnListNewspapersFinishedListener {

	void onLoadNewspapersFinished(List<Newspaper> newspapers, Newspaper defaultNewspaper);
}
