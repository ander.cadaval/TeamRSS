package com.acf.teamrss.presenters;

public interface ListNewsPresenter {

	void onResume(String newspaperTitle, String newspaperUrl);
	
	void onItemClicked(String newspaperTitle, String newLink);
}
