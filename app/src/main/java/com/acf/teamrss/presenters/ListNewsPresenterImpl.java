package com.acf.teamrss.presenters;

import com.acf.teamrss.interactors.NewsInteractor;
import com.acf.teamrss.interactors.NewsInteractorImpl;
import com.acf.teamrss.listeners.OnListNewsFinishedListener;
import com.acf.teamrss.models.New;
import com.acf.teamrss.views.ListNewsView;
import com.android.volley.AuthFailureError;

import java.util.HashMap;
import java.util.List;

public class ListNewsPresenterImpl implements ListNewsPresenter, OnListNewsFinishedListener {

	/*
	 * Properties
	 */

	private ListNewsView listNewsView;
	private NewsInteractor newsInteractor;

	/*
	 * Constructors
	 */

	public ListNewsPresenterImpl(ListNewsView listNewsView) {
		super();
		
        this.listNewsView = listNewsView;
        this.newsInteractor = new NewsInteractorImpl();
        
	}
	
	/*
	 * ListNewsPresenter methods
	 */
	
	@Override
	public void onResume(String newspaperTitle, String nwspaperUrl) {
		try {
			this.newsInteractor.loadNews(newspaperTitle, nwspaperUrl, this);
		} catch (AuthFailureError authFailureError) {
			authFailureError.printStackTrace();
		}
	}

	@Override
	public void onItemClicked(String newspaperTitle, String newLink) { this.newsInteractor.selectNew(newspaperTitle, newLink, this); }
	
	/*
	 * OnListNewFinishedListener methods
	 */
	
	@Override
	public void onLoadNewsFinished(List<New> news) {	this.listNewsView.setNews(news); }

	@Override
	public void onSelectNewFinished(HashMap<String, String> extras) { this.listNewsView.navigateToNew(extras); }

}
