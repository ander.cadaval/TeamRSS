package com.acf.teamrss.presenters;

import android.content.Context;

import com.acf.teamrss.interactors.WelcomeInteractor;
import com.acf.teamrss.interactors.WelcomeInteractorImpl;
import com.acf.teamrss.listeners.OnWelcomeFinishedListener;
import com.acf.teamrss.views.WelcomeView;


public class WelcomePresenterImpl implements WelcomePresenter, OnWelcomeFinishedListener {

	/*
	 * PROPIEDADES
	 */
	
	private WelcomeView welcomeView;
	private WelcomeInteractor welcomeInteractor;
	
	/*
	 * CONSTRUCTORES
	 */
	
	public WelcomePresenterImpl(WelcomeView welcomeView, Context context) {
		super();
		
        this.welcomeView = welcomeView;
        this.welcomeInteractor = new WelcomeInteractorImpl(context);
        
	}
	
	/*
	 * METODOS WelcomePresenter
	 */
	
	@Override
	public void checkConnection() {
		this.welcomeInteractor.checkConnection(this);
	}
	
	/*
	 * METODOS OnWelcomeFinishedListener
	 */

	@Override
	public void onConnected() {
		this.welcomeView.navigateToListNewspapers();
	}

	@Override
	public void onNotConnected() {
		this.welcomeView.openNotNetworkDialog();
	}
}
