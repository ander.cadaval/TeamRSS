package com.acf.teamrss.presenters;

import com.acf.teamrss.interactors.NewspapersInteractor;
import com.acf.teamrss.interactors.NewspapersInteractorImpl;
import com.acf.teamrss.listeners.OnListNewspapersFinishedListener;
import com.acf.teamrss.models.Newspaper;
import com.acf.teamrss.views.MainActivityView;

import java.util.List;

public class MainActivityPresenterImpl implements MainActivityPresenter, OnListNewspapersFinishedListener {

	/*
	 * Properties
	 */
	
	private MainActivityView mainActivityView;
	private NewspapersInteractor newspapersInteractor;
	
	/*
	 * Constructors
	 */
	
	public MainActivityPresenterImpl(MainActivityView mainActivityView) {
		super();
		
        this.mainActivityView = mainActivityView;
        this.newspapersInteractor = new NewspapersInteractorImpl();
        
	}
	
	/*
	 * ListNewspapersPresenter methods
	 */
	
	@Override
	public void loadNewspapers() { this.newspapersInteractor.loadNewspapers(this); }

	
	/*
	 * OnListNewspapersFinishedListener methods
	 */
	
	@Override
	public void onLoadNewspapersFinished(List<Newspaper> newspapers, Newspaper defaultNewspaper) {	this.mainActivityView.setNewspapers(newspapers, defaultNewspaper); }
}
