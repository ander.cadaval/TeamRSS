package com.acf.teamrss.views;

import com.acf.teamrss.models.New;

import java.util.HashMap;
import java.util.List;

public interface ListNewsView {

	void setNews(List<New> news);
	
	void navigateToNew(HashMap<String, String> extras);
}
