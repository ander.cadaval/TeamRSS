package com.acf.teamrss.views;

import com.acf.teamrss.models.Newspaper;

import java.util.List;

public interface MainActivityView {

	void setNewspapers(List<Newspaper> newspapers, Newspaper defaultNewspaper);
}
