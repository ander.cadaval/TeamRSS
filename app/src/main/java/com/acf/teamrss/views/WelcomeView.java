package com.acf.teamrss.views;

public interface WelcomeView {

	void navigateToListNewspapers();

	void openNotNetworkDialog();
}
