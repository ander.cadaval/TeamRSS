package com.acf.teamrss.util;

import com.acf.teamrss.R;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class Constants {
    //public static final int APP_MIN_VERSION = android.os.Build.VERSION_CODES.GINGERBREAD_MR1;

    //public static final int SPLASH_DISPLAY_LENTH = 2000;

    public static final Map<Integer, Object[]> NEWSPAPERS_MAP;
    static {
        Map<Integer, Object[]> aMap = new LinkedHashMap<>();

        aMap.put(R.string.aupa_athletic, new Object[] {R.string.aupa_athletic, R.mipmap.aupa_athletic, "http://www.aupaathletic.com/xml/noticias.xml"});
        aMap.put(R.string.el_correo, new Object[] {R.string.el_correo, R.mipmap.el_correo, "http://www.elcorreo.com/bizkaia/rss/2.0?seccion=athletic"});
        aMap.put(R.string.deia, new Object[] {R.string.deia, R.mipmap.deia, "http://www.deia.com/rss/athletic.xml"});
        aMap.put(R.string.mundo_deportivo, new Object[] {R.string.mundo_deportivo, R.mipmap.mundo_deportivo, "http://www.mundodeportivo.com/feed/rss/futbol/athletic-bilbao"});
        aMap.put(R.string.marca, new Object[] {R.string.marca, R.mipmap.marca, "http://estaticos.marca.com/rss/futbol_equipos_athletic.xml"});
        aMap.put(R.string.as, new Object[] {R.string.as, R.mipmap.as, "http://masdeporte.as.com/tag/rss/athletic/a"});
        aMap.put(R.string.eitb, new Object[] {R.string.eitb, R.mipmap.eitb, "http://www.eitb.eus/es/rss/deportes/futbol/athletic/"});

        NEWSPAPERS_MAP = Collections.unmodifiableMap(aMap);
    }
}
